/******************************************************************************
 *  Copyright (C) 2018 by Mostafa Khattat <mostafa@khattat.nl>                *
 *                                                                            *
 *  This library is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU Lesser General Public License as published  *
 *  by the Free Software Foundation; either version 2 of the License or (at   *
 *  your option) any later version.                                           *
 *                                                                            *
 *  This library is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
 *  Library General Public License for more details.                          *
 *                                                                            *
 *  You should have received a copy of the GNU Lesser General Public License  *
 *  along with this library; see the file COPYING.LIB.                        *
 *  If not, see <http://www.gnu.org/licenses/>.                               *
 *****************************************************************************/

#ifndef FuzzySearch_H
#define FuzzySearch_H

#include <KRunner/AbstractRunner>
#include <QMimeDatabase>


class FuzzySearch : public Plasma::AbstractRunner
{
    Q_OBJECT

public:
    FuzzySearch(QObject *parent, const QVariantList &args);
    ~FuzzySearch() {}

    void match(Plasma::RunnerContext &);
    void run(const Plasma::RunnerContext &, const Plasma::QueryMatch &);
    void reloadConfiguration();
    QIcon icon_for_filename(const QString &filename);
    QString exec(const char* cmd);

private:
    QMimeDatabase mime_database;
    int depth_fs;
    bool fs_triggerKey_bool;
    QString fs_triggerKey;
    bool searchHidden;
    int depthH_fs;
    bool fsH_triggerKey_bool;
    QString fsH_triggerKey;
    QString searchFolders;
    int maxResult;
    int threads;
};

K_EXPORT_PLASMA_RUNNER(fuzzysearch, FuzzySearch)

#endif
