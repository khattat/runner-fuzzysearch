/******************************************************************************
 *  Copyright (C) 2018 by Mostafa Khattat <mostafa@khattat.nl>                *
 *                                                                            *
 *  This library is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU Lesser General Public License as published  *
 *  by the Free Software Foundation; either version 2 of the License or (at   *
 *  your option) any later version.                                           *
 *                                                                            *
 *  This library is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
 *  Library General Public License for more details.                          *
 *                                                                            *
 *  You should have received a copy of the GNU Lesser General Public License  *
 *  along with this library; see the file COPYING.LIB.                        *
 *  If not, see <http://www.gnu.org/licenses/>.                               *
 *****************************************************************************/

#include "main.h"
#include "config/fuzzysearch_config.h"

#include <KLocalizedString>
#include <QApplication>
#include <QClipboard>
#include <QString>
#include <QStyle>
#include <QFileInfo>
#include <QDir>
#include <QFileInfo>

#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

FuzzySearch::FuzzySearch(QObject *parent, const QVariantList &args)
    : Plasma::AbstractRunner(parent, args)
{
    Q_UNUSED(args);
    setObjectName(QLatin1String("FuzzySearch"));
    reloadConfiguration();
    setHasRunOptions(true);
    setIgnoredTypes(Plasma::RunnerContext::None);    
    setSpeed(AbstractRunner::SlowSpeed);
    setPriority(HighestPriority);
    QString description = i18n("Perfoms a fuzzy search among the files and directories.");
    addSyntax(Plasma::RunnerSyntax(QString(fs_triggerKey + " :q:"), description));
    addSyntax(Plasma::RunnerSyntax(QString(fsH_triggerKey + " :q:"), description));
}

QString FuzzySearch::exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result = "";
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) return QString::fromStdString(result);
    int count = 0;
    while (!feof(pipe.get()) && count < maxResult) {
        if (fgets(buffer.data(), 128, pipe.get()) != nullptr) {
            result += buffer.data();
            count++;
        }
    }
    return QString::fromStdString(result);
}

QIcon FuzzySearch::icon_for_filename(const QString &filename)
{
  QIcon icon;
  QList<QMimeType> mime_types = mime_database.mimeTypesForFileName(filename);
  for (int i=0; i < mime_types.count() && icon.isNull(); i++)
    icon = QIcon::fromTheme(mime_types[i].iconName());

  if (icon.isNull())
    return QApplication::style()->standardIcon(QStyle::SP_FileIcon);
  else
    return icon;
}

void FuzzySearch::match(Plasma::RunnerContext &context)
{
    QString text = context.query();
    QString homeDir = QDir::homePath();
    QString fd;
    QString fzf;
    bool searh = true;
    bool enableHidden = searchHidden;
        
    if (!context.isValid()) return;
    
    if (fs_triggerKey_bool && !text.startsWith(fs_triggerKey + " ", Qt::CaseInsensitive)) {
       searh = false;
    }
    
    if (searchHidden && fsH_triggerKey_bool && !text.startsWith(fsH_triggerKey + " ", Qt::CaseInsensitive)) {
        enableHidden = false;
    }
    
    if (enableHidden) {
        text.remove(0, fsH_triggerKey.length());
        fd = "fd -j " + QString::number(threads) + " -d " + (depthH_fs == 0 ? "none" : QString::number(depthH_fs)) + " -H . '" + searchFolders + "'";
    } else if (searh && !enableHidden) {
        fd = "fd -j " + QString::number(threads) + " -d " + (depth_fs == 0 ? "none" : QString::number(depth_fs)) + " . '" + searchFolders + "'";
        text.remove(0, fs_triggerKey.length());
    } else {
        return;
    }
    
        
    if (text.isEmpty())
        return;
    
    fzf = " | fzf -f '" + text + "'";
    float relevance = 1;
    QString res;
    QString q = fd + fzf;
    QStringList output;
    res = exec(q.toLocal8Bit().constData());
    output = res.split('\n');        
    int count = maxResult;
    QList<Plasma::QueryMatch> matches;
    if (output.size() < maxResult) {
        count = output.size();
    } else {
        for (int i = 0; i < count; i++) {
            if (!context.isValid()) {
//                 std::cout << "context is not valid" << std::endl;
                return;
            }
            relevance -= 0.01;
            Plasma::QueryMatch match(this);
            match.setRelevance(relevance);
            QString tmp = output.at(i);
            QFileInfo file(tmp);
            match.setIcon(icon_for_filename(file.fileName()));
            tmp.replace(homeDir + "/", "~/", Qt::CaseInsensitive);
            match.setText(tmp);
            matches.append(match);
        }
    }
    context.addMatches(matches);
}

void FuzzySearch::run(const Plasma::RunnerContext &context, const Plasma::QueryMatch &match)
{
    Q_UNUSED(context);
    QString text = match.text().replace("~/", "~/\"", Qt::CaseInsensitive);
    QString q = "xdg-open " + text + "\"";
    exec(q.toLocal8Bit().constData());
}

void FuzzySearch::reloadConfiguration()
{
    KConfigGroup grp = config();
    
    depth_fs = grp.readEntry(CONFIG_DEPTH, QVariant(5)).toInt();
    fs_triggerKey_bool = grp.readEntry(CONFIG_FS_TRIGGERKEY_BOOL, QVariant(false)).toBool();
    fs_triggerKey = grp.readEntry(CONFIG_FS_TRIGGERKEY, QVariant("")).toString();
    searchHidden = grp.readEntry(CONFIG_SEARCH_HIDDEN, QVariant(false)).toBool();
    depthH_fs = grp.readEntry(CONFIG_DEPTH_H, QVariant(4)).toInt();
    fsH_triggerKey_bool = grp.readEntry(CONFIG_FSH_TRIGGERKEY_BOOL, QVariant(true)).toBool();
    fsH_triggerKey = grp.readEntry(CONFIG_FSH_TRIGGERKEY, QVariant("h")).toString();
    searchFolders = grp.readEntry(CONFIG_SEARCH_FOLDERS, QVariant(QDir::homePath())).toString();
    searchFolders = searchFolders.trimmed().replace("\n", "' '", Qt::CaseInsensitive);
    maxResult = grp.readEntry(CONFIG_MAX_RESULT, QVariant(10)).toInt();
    threads = grp.readEntry(CONFIG_THREADS, QVariant(0)).toInt();
}

#include "moc_main.cpp"
