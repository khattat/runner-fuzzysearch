# Fuzzy Search plugin for KRunner



This is a simple fuzzy search for files and directories based on their names. It depends on `fzf` and `fd` to do the searching. 

## Requirements

[fzf](https://github.com/junegunn/fzf)

[fd](https://github.com/sharkdp/fd)

`extra-cmake-modules`

The output of `cmake` makes it clear if you need to install any extra packages.



## Installation

You can manually try to compile and install the plugin or run `install.sh` script:

```bash
mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=`kf5-config --prefix` -DQT_PLUGIN_INSTALL_DIR=`kf5-config --qt-plugins`
make 
sudo make install
kquitapp krunner
krunner &
```

 