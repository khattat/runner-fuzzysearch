/******************************************************************************
 *  Copyright (C) 2018 by Mostafa Khattat <mostafa@khattat.nl>                *
 *                                                                            *
 *  This library is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU Lesser General Public License as published  *
 *  by the Free Software Foundation; either version 2 of the License or (at   *
 *  your option) any later version.                                           *
 *                                                                            *
 *  This library is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
 *  Library General Public License for more details.                          *
 *                                                                            *
 *  You should have received a copy of the GNU Lesser General Public License  *
 *  along with this library; see the file COPYING.LIB.                        *
 *  If not, see <http://www.gnu.org/licenses/>.                               *
 *****************************************************************************/

#include "fuzzysearch_config.h"
#include <KSharedConfig>
#include <KPluginFactory>
#include <krunner/abstractrunner.h>
#include <QDir>
#include <QSignalMapper>
#include <QGroupBox>


K_PLUGIN_FACTORY(FuzzySearchConfigFactory, registerPlugin<FuzzySearchConfig>("kcm_krunner_fuzzysearch");)

FuzzySearchConfigForm::FuzzySearchConfigForm(QWidget* parent) : QWidget(parent)
{
    setupUi(this);
}

FuzzySearchConfig::FuzzySearchConfig(QWidget* parent, const QVariantList& args) :
        KCModule(parent, args)
{
    m_ui = new FuzzySearchConfigForm(this);
    QGridLayout* layout = new QGridLayout(this);
    layout->addWidget(m_ui, 0, 0);
    
    connect(m_ui->depth_fs, SIGNAL(valueChanged(int)), this, SLOT(changed()));
    connect(m_ui->fs_triggerKey_bool, SIGNAL(toggled(bool)), this, SLOT(changed()));
    connect(m_ui->fs_triggerKey_bool, &QGroupBox::toggled, this, [=]{
            m_ui->fs_triggerKey_bool->isChecked() ? m_ui->fs_triggerKey->setText("fs") : m_ui->fs_triggerKey->setText("");});
    connect(m_ui->fs_triggerKey, SIGNAL(textChanged(QString)), this, SLOT(changed()));
    connect(m_ui->searchHidden, SIGNAL(toggled(bool)), this, SLOT(changed()));
    connect(m_ui->depthH_fs, SIGNAL(valueChanged(int)), this, SLOT(changed()));
    connect(m_ui->fsH_triggerKey_bool, SIGNAL(toggled(bool)), this,SLOT(changed()));
    connect(m_ui->fsH_triggerKey_bool, &QGroupBox::toggled, this, [=]{
            m_ui->fsH_triggerKey_bool->isChecked() ? m_ui->fsH_triggerKey->setText("h") : m_ui->fsH_triggerKey->setText("");});
    connect(m_ui->fsH_triggerKey, SIGNAL(textChanged(QString)), this,SLOT(changed()));
    connect(m_ui->searchFolders, SIGNAL(textChanged()), this, SLOT(changed()));
    connect(m_ui->maxResult, SIGNAL(valueChanged(int)), this, SLOT(changed()));
    connect(m_ui->threads, SIGNAL(valueChanged(int)), this, SLOT(changed()));
    
    load();
}

void FuzzySearchConfig::load()
{
    KCModule::load();
    
    KSharedConfig::Ptr cfg = KSharedConfig::openConfig(QStringLiteral("krunnerrc"));
    KConfigGroup grp = cfg->group("Runners");
    grp = KConfigGroup(&grp, "FuzzySearch");
    
    QVariant depth_fs = grp.readEntry(CONFIG_DEPTH, QVariant(5));
    QVariant fs_triggerKey_bool = grp.readEntry(CONFIG_FS_TRIGGERKEY_BOOL, QVariant(false));
    QVariant fs_triggerKey = grp.readEntry(CONFIG_FS_TRIGGERKEY, QVariant(""));
    QVariant searchHidden = grp.readEntry(CONFIG_SEARCH_HIDDEN, QVariant(false));
    QVariant depthH_fs = grp.readEntry(CONFIG_DEPTH_H, QVariant(4));
    QVariant fsH_triggerKey_bool = grp.readEntry(CONFIG_FSH_TRIGGERKEY_BOOL, QVariant(true));
    QVariant fsH_triggerKey = grp.readEntry(CONFIG_FSH_TRIGGERKEY, QVariant("h"));
    QVariant searchFolders = grp.readEntry(CONFIG_SEARCH_FOLDERS, QVariant(QDir::homePath()));
    QVariant maxResult = grp.readEntry(CONFIG_MAX_RESULT, QVariant(10));
    QVariant threads = grp.readEntry(CONFIG_THREADS, QVariant(0));
    
    
    m_ui->depth_fs->setValue(depth_fs.toInt());
    m_ui->fs_triggerKey_bool->setChecked(fs_triggerKey_bool.toBool());
    m_ui->fs_triggerKey->setText(fs_triggerKey.toString());
    m_ui->searchHidden->setChecked(searchHidden.toBool());
    m_ui->depthH_fs->setValue(depthH_fs.toInt());
    m_ui->fsH_triggerKey_bool->setChecked(fsH_triggerKey_bool.toBool());
    m_ui->fsH_triggerKey->setText(fsH_triggerKey.toString());
    m_ui->searchFolders->setPlainText(searchFolders.toString());
    m_ui->maxResult->setValue(maxResult.toInt());
    m_ui->threads->setValue(threads.toInt());


    emit changed(false);
}

void FuzzySearchConfig::save()
{
    KCModule::save();

    KSharedConfig::Ptr cfg = KSharedConfig::openConfig(QStringLiteral("krunnerrc"));
    KConfigGroup grp = cfg->group("Runners");
    grp = KConfigGroup(&grp, "FuzzySearch");

    grp.writeEntry(CONFIG_DEPTH, QString::number(m_ui->depth_fs->value()));
    grp.writeEntry(CONFIG_FS_TRIGGERKEY_BOOL, boolToString(m_ui->fs_triggerKey_bool->isChecked()));
    grp.writeEntry(CONFIG_FS_TRIGGERKEY, m_ui->fs_triggerKey->text());
    grp.writeEntry(CONFIG_SEARCH_HIDDEN, boolToString(m_ui->searchHidden->isChecked()));
    grp.writeEntry(CONFIG_DEPTH_H, QString::number(m_ui->depthH_fs->value()));
    grp.writeEntry(CONFIG_FSH_TRIGGERKEY_BOOL, boolToString(m_ui->fsH_triggerKey_bool->isChecked()));
    grp.writeEntry(CONFIG_FSH_TRIGGERKEY, m_ui->fsH_triggerKey->text());
    grp.writeEntry(CONFIG_SEARCH_FOLDERS, m_ui->searchFolders->toPlainText());
    grp.writeEntry(CONFIG_MAX_RESULT, QString::number(m_ui->maxResult->value()));
    grp.writeEntry(CONFIG_THREADS, QString::number(m_ui->threads->value()));

    emit changed(false);
}

void FuzzySearchConfig::defaults()
{
    KCModule::defaults();

    m_ui->depth_fs->setValue(5);
    m_ui->fs_triggerKey_bool->setChecked(false);
    m_ui->fs_triggerKey->setText("");
    m_ui->searchHidden->setChecked(false);
    m_ui->depthH_fs->setValue(4);
    m_ui->fsH_triggerKey_bool->setChecked(true);
    m_ui->fsH_triggerKey->setText("h");
    m_ui->searchFolders->setPlainText(QDir::homePath());
    m_ui->maxResult->setValue(10);
    m_ui->threads->setValue(0);

}

#include "fuzzysearch_config.moc"
 
