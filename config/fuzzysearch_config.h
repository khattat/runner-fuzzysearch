/******************************************************************************
 *  Copyright (C) 2018 by Mostafa Khattat <mostafa@khattat.nl>                *
 *                                                                            *
 *  This library is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU Lesser General Public License as published  *
 *  by the Free Software Foundation; either version 2 of the License or (at   *
 *  your option) any later version.                                           *
 *                                                                            *
 *  This library is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
 *  Library General Public License for more details.                          *
 *                                                                            *
 *  You should have received a copy of the GNU Lesser General Public License  *
 *  along with this library; see the file COPYING.LIB.                        *
 *  If not, see <http://www.gnu.org/licenses/>.                               *
 *****************************************************************************/

#ifndef FUZZYSEARCHCONFIG_H
#define FUZZYSEARCHCONFIG_H

#include "ui_fuzzysearch_config.h"
#include <KCModule>

static const char CONFIG_DEPTH[] = "depth";
static const char CONFIG_FS_TRIGGERKEY_BOOL[] = "fs_triggerKey_bool";
static const char CONFIG_FS_TRIGGERKEY[] = "fs_TriggerKey";
static const char CONFIG_SEARCH_HIDDEN[] = "searchHidden";
static const char CONFIG_DEPTH_H[] = "depthH";
static const char CONFIG_FSH_TRIGGERKEY_BOOL[] = "fsH_TriggerKey_bool";
static const char CONFIG_FSH_TRIGGERKEY[] = "fsH_TriggerKey";
static const char CONFIG_SEARCH_FOLDERS[] = "searchFolders";
static const char CONFIG_MAX_RESULT[] = "maxResult";
static const char CONFIG_THREADS[] = "threads";


class FuzzySearchConfigForm : public QWidget, public Ui::FuzzySearchConfigUi
{
    Q_OBJECT

public:
    explicit FuzzySearchConfigForm(QWidget* parent);
};

class FuzzySearchConfig : public KCModule
{
    Q_OBJECT

public:
    explicit FuzzySearchConfig(QWidget* parent = 0, const QVariantList& args = QVariantList());
    
public Q_SLOTS:
    void save();
    void load();
    void defaults();

private:
    FuzzySearchConfigForm* m_ui;
};


inline const char * const boolToString(bool b)
{
  return b ? "true" : "false";
}


#endif 
